#include <iostream>
#include <limits>

using namespace std;

int main()
{
    cout << "Enter a number: ";

    int num{ 0 };
    cin >> num;

    cout << "Double that number is: " << num * 2 << '\n';
    return 0;
}
