#include <iostream>

namespace foo
{
    int doSomething(int x, int y)
    {
        return x + y;
    }
}

namespace goo
{
    int doSomething(int x, int y)
    {
        return x - y;
    }
}

void doSomething()
{
    std::cout << "global\n";
}

auto add(int x, int y) -> int;

int main()
{
    std::cout << foo::doSomething(4, 3) << '\n';
    std::cout << goo::doSomething(4, 3) << '\n';
    doSomething();
    ::doSomething();

    // 类型别名
    using int8_t = char;
    int8_t t8 = 9;

    std::cout << "t8 = " << static_cast<int>(t8) << '\n';

    return 0;
}
