TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -pedantic-errors -Wall -Weffc++ -Wextra -Wsign-conversion -Werror -std=c++11

SOURCES += main.cpp \
    add.cpp

HEADERS += \
    add.h
