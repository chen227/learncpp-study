#include <iostream>
#include "add.h"
#include <bitset>

int main()
{
    std::bitset<8> bits{ 0x5 }; // we need 8 bits, start with bit pattern 0000 0101
    std::cout << "All the bits: " << bits << '\n';

    bits.set(3); // set bit position 3 to 1 (now we have 0000 1101)
    std::cout << "set the bits: " << bits << '\n';

    bits.flip(4); // flip bit 4 (now we have 0001 1101)
    std::cout << "flip the bits: " << bits << '\n';

    bits.reset(4); // set bit 4 back to 0 (now we have 0000 1101)
    std::cout << "reset the bits: " << bits << '\n';

    std::cout << "All the bits: " << bits << '\n';
    std::cout << "Bit 3 has value: " << bits.test(3) << '\n';
    std::cout << "Bit 4 has value: " << bits.test(4) << '\n';

    std::bitset<8> isHungry{ 0x1 };
    std::bitset<8> me{}; // all flags/options turned off to start
    std::cout << "I am isHungry? " << (me & isHungry).any() << '\n';

    constexpr std::uint_fast32_t redBits{ 0xFF000000 };
    constexpr std::uint_fast32_t greenBits{ 0x00FF0000 };
    constexpr std::uint_fast32_t blueBits{ 0x0000FF00 };
    constexpr std::uint_fast32_t alphaBits{ 0x000000FF };

    std::cout << "Enter a 32-bit RGBA color value in hexadecimal (e.g. FF7F3300): ";
    std::uint_fast32_t pixel{};
    std::cin >> std::hex >> pixel;

    std::uint_fast8_t red{ static_cast<std::uint_fast8_t>((pixel & redBits) >> 24) };
    std::uint_fast8_t green{ static_cast<std::uint_fast8_t>((pixel & greenBits) >> 16) };
    std::uint_fast8_t blue{ static_cast<std::uint_fast8_t>((pixel & blueBits) >> 8) };
    std::uint_fast8_t alpha{ static_cast<std::uint_fast8_t>(pixel & alphaBits) };

    std::cout << "Your color contains:\n";
    std::cout << std::hex; // print the following values in hex
    std::cout << static_cast<int>(red) << " red\n";
    std::cout << static_cast<int>(green) << " green\n";
    std::cout << static_cast<int>(blue) << " blue\n";
    std::cout << static_cast<int>(alpha) << " alpha\n";

    return 0;
}
