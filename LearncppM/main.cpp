#include <iostream>
#include <iterator>
#include <memory>

class Resource
{
public:
    Resource() { std::cout << "Resource acquired\n"; }
    ~Resource() { std::cout << "Resource destroyed\n"; }
    friend std::ostream& operator<<(std::ostream& out, const Resource &res)
    {
        (void)res;
        out << "I am a resource\n";
        return out;
    }
};

int main()
{
    //    M.6		std :: unique_ptr
    std::unique_ptr<Resource> res{ new Resource() };
    if (res) // use implicit cast to bool to ensure res contains a Resource
        std::cout << *res << '\n'; // print the Resource that res is owning

    return 0;
}
