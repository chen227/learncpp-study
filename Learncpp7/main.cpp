#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>

#define NDEBUG

void cleanup()
{
    std::cout << "cleanup!\n";
}

double calculateTimeUntilObjectHitsGround(double initialHeight, double gravity)
{
  assert(gravity > 0.0 && "gravity could not be A negative number"); // The object won't reach the ground unless there is positive gravity.

  if (initialHeight <= 0.0)
  {
    // The object is already on the ground. Or buried.
    return 0.0;
  }

  return std::sqrt((2.0 * initialHeight) / gravity);
}

//static_assert(sizeof(long) == 8, "long must be 8 bytes");
static_assert(sizeof(int) == 4, "int must be 4 bytes");

int main()
{
//    int x{};
//    std::cin >> x;
//    std::cout << "x " << x << '\n';

    std::atexit(cleanup);

    std::cout << 1 << '\n';

//    calculateTimeUntilObjectHitsGround(100.0, -9.8);

    std::exit(0);
//    std::abort();
//    std::terminate();

    std::cout << 2 << '\n';

    return 0;
}
