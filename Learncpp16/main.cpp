#include <iostream>
#include <iterator>
#include <vector>
#include <functional>
#include <string>
#include <initializer_list>

class IntArray
{
private:
    int m_length{};
    int* m_data{};

public:
    IntArray() = default;

    IntArray(int length):
        m_length{ length },
        m_data{ new int[length]{} }
    {}

    IntArray(std::initializer_list<int> list):  // allow IntArray to be initialized via list initialization
        IntArray(static_cast<int>(list.size()))
    {
        int count{ 0 };
        for(auto element : list)
        {
            m_data[count] = element;
            ++count;
        }
    }

    ~IntArray()
    {
        delete[] m_data;
    }

    IntArray(const IntArray&) = delete;
    IntArray& operator=(const IntArray& list) = delete;

    int& operator[](int index)
    {
        return m_data[index];
    }

    int getLength() const { return m_length; }
};

int main()
{
    // 16.3		聚合
    std::string tom{ "Tom" };
    std::vector<std::reference_wrapper<std::string>> names{ tom };

    std::string jim{ "Jim" };
    names.push_back(jim);

    for(auto name : names)
    {
        name.get() += " Beam";
    }

    std::cout << jim << '\n';

    IntArray array{ 1, 2, 3 };
    for(int count{ 0 }; count < array.getLength(); ++count)
        std::cout << array[count] << ' ';

    return 0;
}
