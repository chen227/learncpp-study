#include <iostream>
#include "add.h"
#include <bitset>

int main()
{
    std::cout << true << '\n';
    std::cout << false << '\n';

    std::cout << std::boolalpha;

    std::cout << true << '\n';
    std::cout << false << '\n';

    std::cout << std::noboolalpha;

    std::cout << true << '\n';
    std::cout << false << '\n';

    int x { 12 };
    std::cout << x << '\n'; // decimal (by default)
    std::cout << "std::hex " << std::hex << x << '\n'; // hexadecimal
    std::cout << x << '\n'; // now hexadecimal
    std::cout << "std::oct " << std::oct << x << '\n'; // octal
    std::cout << "std::dec " << std::dec << x << '\n'; // return to decimal
    std::cout << x << '\n'; // decimal

    // std::bitset<8> means we want to store 8 bits
    // ob 符合C++14，不支持C++11
    std::bitset<8> bin1{ 11000101 }; // binary literal for binary 1100 0101 0b11000101
    std::bitset<8> bin2{ 0xC5 }; // hexadecimal literal for binary 1100 0101

    std::cout << bin1 << ' ' << bin2 << '\n';
    std::cout << std::bitset<4>{ 1010 } << '\n'; // we can also print from std::bitset directly

    constexpr double gravity { 9.8 }; // ok, the value of 9.8 can be resolved at compile-time
    constexpr int sum { 4 + 5 }; // ok, the value of 4 + 5 can be resolved at compile-time

    (void)gravity;
    (void)sum;
    std::cout << "Enter your age: ";
    int age{};
    std::cin >> age;
//    constexpr int myAge { age }; // not okay, age can not be resolved at compile-time

    return 0;
}
